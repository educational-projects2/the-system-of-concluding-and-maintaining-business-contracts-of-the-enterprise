﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EconomyPgSQL
{
    /// <summary>
    /// Логика взаимодействия для Editor.xaml
    /// </summary>
    public partial class Editor : Window
    {
        public Connector connector;
        public Editor(Connector conn)
        {
            this.connector = conn;
            InitializeComponent();

            ContTransaction.ItemsSource = GetRows("SELECT subject FROM transactions");
            ContTransaction.SelectedItem = ContTransaction.Items[0];

            TranSupp.ItemsSource = GetRows("SELECT name FROM suppliers");
            TranSupp.SelectedItem = TranSupp.Items[0];

            TranDep.ItemsSource = GetRows("SELECT name FROM departments");
            TranDep.SelectedItem = TranDep.Items[0];

            DepContract.ItemsSource = GetRows("SELECT name FROM contracts");
            DepContract.SelectedItem = DepContract.Items[0];

            DelSupp.ItemsSource = GetRows("SELECT name FROM suppliers");
            DelSupp.SelectedItem = DelSupp.Items[0];

            ContractDel.ItemsSource = GetRows("SELECT name FROM contracts");
            ContractDel.SelectedItem = ContractDel.Items[0];

            TranDel.ItemsSource = GetRows("SELECT subject FROM transactions");
            TranDel.SelectedItem = TranDel.Items[0];

            DepDel.ItemsSource = GetRows("SELECT name FROM departments");
            DepDel.SelectedItem = DepDel.Items[0];
        }
        private void Show_Suppliers(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT name, property, type_supp, country FROM suppliers");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void Show_Contracts(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT (select t.subject from transactions t where c.tr_number = t.id) as \"subject\", c.name, c.begin_date, c.end_date FROM contracts c");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void Show_Departments(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT d.name, (select c.name from contracts c where c.id = d.contract) as \"contract\", d.chief, d.type_activity, d.country FROM departments d");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void Show_Transactions(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT (SELECT s.name FROM suppliers s WHERE s.id=t.supplier) as \"supplier\", (SELECT d.name FROM departments d WHERE d.id = t.department) as \"department\", subject FROM transactions t");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void insert_supplier(object sender, RoutedEventArgs e)
        {
            string suppName = SuppName.Text;
            string suppProperty = SuppProperty.Text;
            string suppType = SuppProperty.Text;
            string suppCountry = SuppCountry.Text;

            connector.no_result_querry("insert into suppliers(id, name, property, type_supp, country) values ((select max(id)+1 from suppliers),\'"+suppName+ "\',\'" + suppProperty+ "\',\'" + suppType+"\',\'"+suppCountry+"\' )");

            string sql = ("SELECT name, property, type_supp, country FROM suppliers");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void update_supplier(object sender, RoutedEventArgs e)
        {
            string suppName = SuppName.Text;
            string suppProperty = SuppProperty.Text;
            string suppType = SuppType.Text;
            string suppCountry = SuppCountry.Text;
            string suppId = GetRows("select max(id) from suppliers where name = \'"+suppName+"\'")[0].ToString();

            connector.no_result_querry("UPDATE suppliers SET property=\'"+suppProperty+"\', type_supp = \'"+suppType+"\', country = \'"+suppCountry+"\' where id = "+suppId);

            string sql = ("SELECT name, property, type_supp, country FROM suppliers");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }
        public object[] GetRows(string sql)
        {
            DataView suppliers = connector.runQuery(sql);
            DataRowCollection table = suppliers.Table.Rows;

            object[] rows = new object[table.Count];
            for (int i = 0; i < table.Count; i++)
            {
                rows[i] = table[i].ItemArray[0];
                DataRow row = table[i];
            }
            return rows;
        }

        private void insert_contract(object sender, RoutedEventArgs e)
        {
            string contTra = ContTransaction.SelectedItem.ToString();
            string contName = ContName.Text;
            string contBegin = ContBegin.Text;
            string contEnd = ContEnd.Text;
            string trNumber = GetRows("select max(id) from transactions where subject = \'"+contTra+"\'")[0].ToString();

            connector.no_result_querry("insert into contracts(id, tr_number, name, begin_date, end_date) values ((select max(id)+1 from contracts), " + trNumber + ", \'" + contName + "\' ,\'" + contBegin + "\', \'" + contEnd + "\' )");

            string sql = ("SELECT (select t.subject from transactions t where c.tr_number = t.id) as \"subject\", c.name, c.begin_date, c.end_date FROM contracts c");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void update_contract(object sender, RoutedEventArgs e)
        {
            string contTra = ContTransaction.SelectedItem.ToString();
            string contName = ContName.Text;
            string contBegin = ContBegin.Text;
            string contEnd = ContEnd.Text;
            string trNumber = GetRows("select max(id) from transactions where subject = \'" + contTra + "\'")[0].ToString();
            string contId = GetRows("(select max(id) from contracts where name = \'"+contName+"\')")[0].ToString();

            connector.no_result_querry("call update_contracts("+contId+","+trNumber+",\'"+contName+"\',\'"+contBegin+"\',\'"+contEnd+"\')");

            string sql = ("SELECT (select t.subject from transactions t where c.tr_number = t.id) as \"subject\", c.name, c.begin_date, c.end_date FROM contracts c");
            dataGridView1.ItemsSource = connector.runQuery(sql);

        }

        private void insert_transaction(object sender, RoutedEventArgs e)
        {
            string tranSupp = TranSupp.SelectedItem.ToString();
            string tranDep = TranDep.SelectedItem.ToString();
            string tranObj = TranObj.Text;
            string trId = GetRows("select max(id)+1 from transactions")[0].ToString();
            string suppId = GetRows("select max(id) from suppliers where name = \'"+tranSupp+"\'")[0].ToString();
            string depId = GetRows("select max(id) from departments where name = \'" + tranDep + "\'")[0].ToString();

            connector.no_result_querry("call insert_transactions("+trId+","+suppId+","+depId+",\'"+tranObj+"\')");

            string sql = ("SELECT (SELECT s.name FROM suppliers s WHERE s.id=t.supplier) as \"supplier\", (SELECT d.name FROM departments d WHERE d.id = t.department) as \"department\", subject FROM transactions t");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void update_transaction(object sender, RoutedEventArgs e)
        {
            string tranSupp = TranSupp.SelectedItem.ToString();
            string tranDep = TranDep.SelectedItem.ToString();
            string tranObj = TranObj.Text;
            string trId = GetRows("select max(id) from transactions where subject = \'"+tranObj+"\'")[0].ToString();
            string suppId = GetRows("select max(id) from suppliers where name = \'" + tranSupp + "\'")[0].ToString();
            string depId = GetRows("select max(id) from departments where name = \'" + tranDep + "\'")[0].ToString();
            
            connector.no_result_querry("call update_transactions(" + trId + "," + suppId + "," + depId + ",\'" + tranObj + "\')");

            string sql = ("SELECT (SELECT s.name FROM suppliers s WHERE s.id=t.supplier) as \"supplier\", (SELECT d.name FROM departments d WHERE d.id = t.department) as \"department\", subject FROM transactions t");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void insert_department(object sender, RoutedEventArgs e)
        {
            string depName = DepName.Text;
            string depContract = DepContract.SelectedItem.ToString();
            string depChief = DepChief.Text;
            string depAct = DepActivity.Text;
            string depCountry = DepCountry.Text;
            string depId = GetRows("select max(id)+1 from departments")[0].ToString();
            string contId = GetRows("select max(id) from contracts where name = \'"+depContract+"\'")[0].ToString();

            connector.no_result_querry("call insert_departments("+depId+",\'"+depName+"\', "+contId+", \'"+depChief+"\', \'"+depAct+"\',\'"+depCountry+"\')");
            string sql = ("SELECT d.name, (select c.name from contracts c where c.id = d.contract) as \"contract\", d.chief, d.type_activity, d.country FROM departments d");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void update_department(object sender, RoutedEventArgs e)
        {
            string depName = DepName.Text;
            string depContract = DepContract.SelectedItem.ToString();
            string depChief = DepChief.Text;
            string depAct = DepActivity.Text;
            string depCountry = DepCountry.Text;
            string depId = GetRows("select max(id) from departments where name = \'"+depName+"\'")[0].ToString();
            string contId = GetRows("select max(id) from contracts where name = \'" + depContract + "\'")[0].ToString();

            connector.no_result_querry("call update_departments(" + depId + ",\'" + depName + "\', " + contId + ", \'" + depChief + "\', \'" + depAct + "\',\'" + depCountry + "\')");
            
            string sql = ("SELECT d.name, (select c.name from contracts c where c.id = d.contract) as \"contract\", d.chief, d.type_activity, d.country FROM departments d");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void delete_supplier(object sender, RoutedEventArgs e)
        {
            string suppName = DelSupp.SelectedItem.ToString();
            string suppId = GetRows("select max(id) from suppliers where name = \'"+suppName+"\'")[0].ToString();

            connector.no_result_querry("call delete_suppliers("+suppId+")");

            string sql = ("SELECT name, property, type_supp, country FROM suppliers");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void delete_contract(object sender, RoutedEventArgs e)
        {
            string contName = ContractDel.SelectedItem.ToString();
            string contId = GetRows("select max(id) from contracts where name = \'" + contName + "\'")[0].ToString();

            connector.no_result_querry("call delete_contracts(" + contId + ")");

            string sql = ("SELECT (select t.subject from transactions t where c.tr_number = t.id) as \"subject\", c.name, c.begin_date, c.end_date FROM contracts c");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void delete_transaction(object sender, RoutedEventArgs e)
        {
            string tranObject = TranDel.SelectedItem.ToString();
            string tranId = GetRows("select max(id) from transactions where subject = \'" + tranObject + "\'")[0].ToString();

            connector.no_result_querry("call delete_transactions(" + tranId + ")");

            string sql = ("SELECT (SELECT s.name FROM suppliers s WHERE s.id=t.supplier) as \"supplier\", (SELECT d.name FROM departments d WHERE d.id = t.department) as \"department\", subject FROM transactions t");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void delete_department(object sender, RoutedEventArgs e)
        {
            string depName = DepDel.SelectedItem.ToString();
            string depId = GetRows("select max(id) from departments where name = \'" + depName + "\'")[0].ToString();

            connector.no_result_querry("call delete_departments(" + depId + ")");

            string sql = ("SELECT d.name, (select c.name from contracts c where c.id = d.contract) as \"contract\", d.chief, d.type_activity, d.country FROM departments d");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }
    }
}
