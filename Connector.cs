﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EconomyPgSQL
{
    public class Connector
    {
        private DataSet ds = new DataSet();
        private DataTable dt = new DataTable();

        private static NpgsqlConnection conn;
        public Connector(string login, string pass)
        {
            conn = new NpgsqlConnection("Server=127.0.0.1;Port=5432;User Id="+login+";Password="+pass+";Database=economy;");
            conn.Open();
        }

        public DataView runQuery(string query)
        {
            NpgsqlDataAdapter da = new NpgsqlDataAdapter(query, conn);
            ds.Reset();
            da.Fill(ds);
            dt = ds.Tables[0];
            return dt.DefaultView;
        }
        public void no_result_querry(string sql)
        {
            NpgsqlCommand command = new NpgsqlCommand(sql, conn);
            command.ExecuteNonQuery();
        }
        public void disconnect()
        {
            conn.Close();
        }
    }
}
