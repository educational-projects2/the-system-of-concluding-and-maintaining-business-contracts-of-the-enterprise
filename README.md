# The system of concluding and maintaining business contracts of the enterprise
## Task
Develop a client-server application, the server part of which is implemented on Microsoft
SQL Server or PostgreSQL, which is a domain model in
accordance with the task option. Within a given subject area, implement
a given (according to the variant) scheme of relations, i.e. highlight entities and their attributes, so that
the relationships between entities correspond to the presented scheme. As part of the course work, 
it is necessary to implement and use the following components on the server side when demonstrating the application:
1. Permanent tables and links between them, the number of tables and the presence of links
must correspond to the task, an increase in the number of tables and their fields
is allowed for a more adequate representation of the subject area;
2. Implement at least five queries in the application, including (to demonstrate
work skills)
a. A composite multi-table query with a parameter that includes a join
of tables and a CASE expression;
b. Based on an updating view (multi-table VIEW), in which
the ordering criterion is set by the user when executing;
c. A query containing correlated and uncorrelated subqueries in
the SELECT, FROM and WHERE sections (at least one in each);
d. A multi-table query containing a grouping of records, aggregate
functions and a parameter used in the HAVING section;
e. A query containing the predicate ANY(SOME) or ALL;
3. Create indexes to increase query execution speed;
4. In the table (according to the option), provide a field that is filled
in automatically when the trigger is triggered when adding, updating and deleting
data, be able to demonstrate the operation of the trigger when the
application is running. Triggers should process only those records that were
added, modified or deleted during the current operation (transaction).
5. Add, delete and update operations are implemented in the form of stored procedures
(functions) with parameters for all tables;
6. Implement a separate stored procedure (function) consisting of several
separate operations in the form of a single transaction, which under certain conditions
can be committed or rolled back;
7. In a trigger or stored procedure, implement a cursor to update individual
data;
8. Use your own scalar function in the request (from point 2 or in the additional list), 
and in the stored procedure – a vector (or
tabular) function. Save to database functions
9. Distribution of user rights: provide at least two users with
a different set of privileges. Each set of privileges is designed as a role.
The client must ensure the addition, modification and deletion of data throughout
subject area. Add and edit data in the table in
a separate window.
It is forbidden to specify the values of primary and foreign keys as input data, including for linking tables
– to ensure referential integrity, the user
must select values from the directory, and the corresponding values must
be substituted programmatically (in one way or another – automatically).
The client must be implemented in the means (programming languages) studied
(studied) within the framework of the curriculum.
