﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EconomyPgSQL
{
    /// <summary>
    /// Логика взаимодействия для Authorization.xaml
    /// </summary>
    public partial class Authorization : Window
    {
        public Authorization()
        {
            InitializeComponent();
        }

        private void log_in(object sender, RoutedEventArgs e)
        {
            string inputLogin = Login.Text;
            string inputPassword = Password.Text;

            if(inputLogin == "admin" && inputPassword == "1111")
            {
                MainWindow mw = new MainWindow(new Connector("admin", "1111"));
                mw.Show();
                this.Close();
            }
            else if(inputLogin == "user1" && inputPassword == "0000")
            {
                UserWindow uw = new UserWindow(new Connector("user1", "0000"));
                uw.Show();
                this.Close();
            }
            else
            {
                Mess.Content = "Your login or password incorrect";
            }
        }
    }
}
