﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Drawing;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Npgsql;
using System.Runtime.Remoting.Metadata.W3cXsd2001;


namespace EconomyPgSQL
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Connector connector;
        public TextBox tb;

        private DataSet ds = new DataSet();
        private DataTable dt = new DataTable();
        public MainWindow(Connector conn)
        {
            this.connector = conn;
            InitializeComponent();

            connector = new Connector("admin","1111");

            SupplierBox.ItemsSource = GetRows("SELECT name FROM suppliers");
            SupplierBox.SelectedItem = SupplierBox.Items[0];

            DepartmentBox.ItemsSource = GetRows("SELECT name FROM departments");
            DepartmentBox.SelectedItem = DepartmentBox.Items[0];

            DeleteObjectBox.ItemsSource = GetRows("SELECT subject FROM transactions");
            DeleteObjectBox.SelectedItem = DeleteObjectBox.Items[0];

            UpdateSupplierBox.ItemsSource = GetRows("SELECT name FROM suppliers");
            UpdateSupplierBox.SelectedItem = UpdateSupplierBox.Items[0];

            UpdateDepartmentBox.ItemsSource = GetRows("SELECT name FROM departments");
            UpdateDepartmentBox.SelectedItem = UpdateDepartmentBox.Items[0];

            UpdateObjectBox.ItemsSource = GetRows("SELECT subject FROM transactions");
            UpdateObjectBox.SelectedItem = UpdateObjectBox.Items[0];

        }
        public object[] GetRows(string sql)
        {
            DataView suppliers = connector.runQuery(sql);
            DataRowCollection table = suppliers.Table.Rows;

            object[] rows = new object[table.Count];
            for (int i = 0; i < table.Count; i++)
            {
                rows[i] = table[i].ItemArray[0];
                DataRow row = table[i];
            }
            return rows;
        }

        private void Show_Suppliers(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT name, property, type_supp, country FROM suppliers");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void disconnect(object sender, RoutedEventArgs e)
        {
            connector.disconnect();
            this.Close();
        }

        private void Show_Contracts(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT (select t.subject from transactions t where c.tr_number = t.id) as \"subject\", c.name, c.begin_date, c.end_date FROM contracts c");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void Show_Departments(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT d.name, (select c.name from contracts c where c.id = d.contract) as \"contract\", d.chief, d.type_activity, d.country FROM departments d");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void Show_Transactions(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT (SELECT s.name FROM suppliers s WHERE s.id=t.supplier) as \"supplier\", (SELECT d.name FROM departments d WHERE d.id = t.department) as \"department\", subject FROM transactions t");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void _2a_click(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT t.subject AS \"Предмет сделки\",c.name AS \"Название договора\",c.begin_date AS \"Дата начала сделки\"\r\nFROM transactions t INNER JOIN contracts c\r\n \tON c.tr_number=t.id\r\n\tWHERE c.id \r\n\tIN (SELECT CASE\r\n\t\t\t\tWHEN c.begin_date>TO_DATE('2021-03-16','YYYY MM DD') then\r\n\t\t\t\t\tc.id\r\n\t\t\t\telse\r\n\t\t\t\t\tNULL\r\n\t\t\tEND\r\n\t\tFROM contracts )");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void _2b_click(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT Поставщик, Поставщик сделки, \"Страна поставщика\" FROM my_view");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void _2c_click(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT c.name AS \"Название договора\",\r\n\t(SELECT max(s.id) from suppliers s where s.id=t.supplier) AS \"Номер поставщика\",\r\n\tt.subject AS \"Предмет сделки\",\r\n\tc.begin_date AS \"Дата заключения\"\r\nFROM (SELECT * FROM contracts ) c,transactions t\r\nWHERE t.supplier IN (SELECT id \r\n\t\t\t\t\t  FROM suppliers \r\n\t\t\t\t\t  WHERE name!='Indian Paper' )\r\n\t AND(c.tr_number = t.id)");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void _2d_click(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT d.name AS \"Название отдела\", d.chief AS \"Начальник\",\r\n\tt.subject AS \"Предмет сделки\"\r\nFROM departments d inner join transactions t \r\nON d.id=t.department\r\nGROUP BY t.id,d.id,d.name,d.chief,t.subject,t.department\r\nHAVING t.id in \r\n\t(select tr_number from contracts group by tr_number having count(tr_number)=1 )");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void _2e_click(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT d.name AS \"Название отдела\"\r\nFROM departments d\r\nWHERE d.contract =ANY(SELECT contract FROM departments)");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void _81_click(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT * FROM transactions t WHERE t.supplier = supplierByName('MashineIN');");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void _82_click(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT * FROM contractsEndDate('2021-04-01');");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void show_all(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT t.id AS \"Номер сделки\",t.subject AS \"Предмет сделки\",s.name AS \"Поставщик\",\r\nd.name AS \"Название отдела\", c.name AS \"Договор\",d.chief AS \"Начальник отдела\"\r\nFROM transactions t, suppliers s, departments d, contracts c\r\nWHERE (t.supplier=s.id)and(t.department=d.id)and(c.tr_number=t.id)and(d.contract=c.id);");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void add_transaction(object sender, RoutedEventArgs e)
        {
            string contentAdd = ObjectBox.Text;
            string suppAdd = SupplierBox.SelectedItem.ToString();
            string depAdd = DepartmentBox.SelectedItem.ToString();
            string suppId = GetRows("select id from suppliers where name = \'"+suppAdd+"\'")[0].ToString();
            string depId = GetRows("select id from departments where name = \'" + depAdd + "\'")[0].ToString();
            string trId = GetRows("select max(id)+1 from transactions")[0].ToString();
            connector.no_result_querry("call insert_transactions("+trId+","+suppId+","+depId+",\'"+contentAdd+"\')");
            string sql = ("SELECT (SELECT s.name FROM suppliers s WHERE s.id=t.supplier) as \"supplier\", (SELECT d.name FROM departments d WHERE d.id = t.department) as \"department\", subject FROM transactions t");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void delete_transaction(object sender, RoutedEventArgs e)
        {
            string objectDel = DeleteObjectBox.SelectedItem.ToString();
            string objId = GetRows("select id from transactions where subject = \'" + objectDel + "\'")[0].ToString();
            connector.no_result_querry("call delete_transactions(" + objId +")");
            string sql = ("SELECT (SELECT s.name FROM suppliers s WHERE s.id=t.supplier) as \"supplier\", (SELECT d.name FROM departments d WHERE d.id = t.department) as \"department\", subject FROM transactions t");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void update_transaction(object sender, RoutedEventArgs e)
        {
            string contentUpd = UpdateObjectBox.SelectedItem.ToString();
            string suppUpd = UpdateSupplierBox.SelectedItem.ToString();
            string depUpd = DepartmentBox.SelectedItem.ToString();
            string suppId = GetRows("select id from suppliers where name = \'" + suppUpd + "\'")[0].ToString();
            string depId = GetRows("select id from departments where name = \'" + depUpd + "\'")[0].ToString();
            string trId = GetRows("select max(id) from transactions where subject = \'"+contentUpd+"\'")[0].ToString();
            connector.no_result_querry("call update_transactions(" + trId + "," + suppId + "," + depId + ",\'" + contentUpd + "\')");
            string sql = ("SELECT (SELECT s.name FROM suppliers s WHERE s.id=t.supplier) as \"supplier\", (SELECT d.name FROM departments d WHERE d.id = t.department) as \"department\", subject FROM transactions t");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void delete_from_all_tables(object sender, RoutedEventArgs e)
        {
            string delID = deleteId.Text;
            connector.no_result_querry("call delete_all_byID("+delID+")");
            string sql = ("SELECT t.id AS \"Номер сделки\",t.subject AS \"Предмет сделки\",s.name AS \"Поставщик\",\r\nd.name AS \"Название отдела\", c.name AS \"Договор\",d.chief AS \"Начальник отдела\"\r\nFROM transactions t, suppliers s, departments d, contracts c\r\nWHERE (t.supplier=s.id)and(t.department=d.id)and(c.tr_number=t.id)and(d.contract=c.id);");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void open_redactor_click(object sender, RoutedEventArgs e)
        {
            Editor editorWindow = new Editor(connector);
            editorWindow.Show();
        }
    }
}
