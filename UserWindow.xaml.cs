﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EconomyPgSQL
{
    /// <summary>
    /// Логика взаимодействия для UserWindow.xaml
    /// </summary>
    public partial class UserWindow : Window
    {
        public Connector connector;
        public UserWindow(Connector conn)
        {
            this.connector = conn;
            InitializeComponent();
        }

        private void Show_Suppliers(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT name, property, type_supp, country FROM suppliers");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void Show_Contracts(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT (select t.subject from transactions t where c.tr_number = t.id) as \"subject\", c.name, c.begin_date, c.end_date FROM contracts c");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void Show_Departments(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT d.name, (select c.name from contracts c where c.id = d.contract) as \"contract\", d.chief, d.type_activity, d.country FROM departments d");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void Show_Transactions(object sender, RoutedEventArgs e)
        {
            string sql = ("SELECT (SELECT s.name FROM suppliers s WHERE s.id=t.supplier) as \"supplier\", (SELECT d.name FROM departments d WHERE d.id = t.department) as \"department\", subject FROM transactions t");
            dataGridView1.ItemsSource = connector.runQuery(sql);
        }

        private void disconnect(object sender, RoutedEventArgs e)
        {
            connector.disconnect();
            this.Close();
        }
    }
}
